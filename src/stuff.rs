use bevy::{prelude::*, sprite::Anchor, time::Stopwatch};
use bevy_asepritesheet::prelude::*;
use bevy_rapier2d::prelude::*;
use rand::*;

use crate::core::{GameTime, WorldCam};

// -------------------------------------------------------------------------------------------------

pub struct StuffPlugin;

#[derive(Component, Default)]
pub struct Stuff;

#[derive(Bundle)]
pub struct StuffBundle {
    pub stuff: Stuff,
    pub rigidbody: RigidBody,
    pub collider: Collider,
    pub mass_props: ColliderMassProperties,
    pub velocity: Velocity,
    pub friction: Friction,
    pub restitution: Restitution,
    pub damping: Damping,
    pub sprite: SpriteSheetBundle,
}

#[derive(Resource, Default)]
pub struct StuffData {
    pub is_loading: bool,
    spritesheet_handle: Handle<SpritesheetData>,
    rain_timer: Stopwatch,
    pub spritesheet: Option<Spritesheet>,
}

// -------------------------------------------------------------------------------------------------

impl Plugin for StuffPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(StuffData::default())
            .add_systems(Update, (load, handle_stuff_rain));
    }
}

impl Default for StuffBundle {
    fn default() -> Self {
        Self {
            stuff: Stuff,
            collider: Collider::ball(12.0),
            mass_props: ColliderMassProperties::Density(1.0),
            friction: Friction::coefficient(0.5),
            restitution: Restitution::coefficient(0.5),
            rigidbody: RigidBody::Dynamic,
            velocity: Velocity::default(),
            damping: Damping::default(),
            sprite: SpriteSheetBundle::default(),
        }
    }
}

// Systems: ----------------------------------------------------------------------------------------

fn load(
    asset_server: Res<AssetServer>,
    mut stuff_data: ResMut<StuffData>,
    spritesheet_assets: Res<Assets<SpritesheetData>>,
    mut atlas_assets: ResMut<Assets<TextureAtlas>>,
) {
    if !stuff_data.is_loading && stuff_data.spritesheet.is_none() {
        stuff_data.is_loading = true;
        stuff_data.spritesheet_handle = asset_server.load("stuff.sprite.json");
        println!("Loading Stuff GFX");
    } else if stuff_data.spritesheet.is_none() {
        if let Some(sheet_data) = spritesheet_assets.get(&stuff_data.spritesheet_handle) {
            let spritesheet = Spritesheet::from_data_image(
                &sheet_data,
                asset_server.load::<Image>(&sheet_data.meta.image),
                Anchor::Center,
                &mut atlas_assets,
            );
            stuff_data.spritesheet = Some(spritesheet);
            stuff_data.is_loading = false;
            println!("Stuff GFX Loaded!");
        }
    }
}

fn handle_stuff_rain(
    mut commands: Commands,
    time: Res<GameTime>,
    mut stuff_data: ResMut<StuffData>,
    cam_query: Query<(&GlobalTransform, &Camera), With<WorldCam>>,
) {
    stuff_data.rain_timer.tick(time.delta());
    let elapsed = stuff_data.rain_timer.elapsed_secs();

    if elapsed >= 0.25 {
        let (cam_trans, cam) = cam_query.single();
        if cam_trans.translation().y > -250.0 {
            return;
        }
        let x_min = cam
            .ndc_to_world(cam_trans, Vec3::new(-1.0, 0.0, 0.0))
            .unwrap_or_default()
            .x;
        let x_max = cam
            .ndc_to_world(cam_trans, Vec3::new(1.0, 0.0, 0.0))
            .unwrap_or_default()
            .x;
        let y_max = cam
            .ndc_to_world(cam_trans, Vec3::new(0.0, 1.0, 0.0))
            .unwrap_or_default()
            .y
            + 10.0;
        for _ in 0..3 {
            if let Some(stuff_spritesheet) = stuff_data.spritesheet.as_ref() {
                if let Ok(stuff) = spawn_stuff(
                    Vec2::new(thread_rng().gen_range(x_min..x_max), y_max),
                    Vec2::new(0.0, -200.0),
                    &stuff_spritesheet,
                ) {
                    commands.spawn(stuff);
                }
            }
            stuff_data.rain_timer.reset();
        }
    }
}

// Utility: ----------------------------------------------------------------------------------------

/// creates a stuff bundle at the specified position, with the specified velocity
pub fn spawn_stuff(pos: Vec2, vel: Vec2, spritesheet: &Spritesheet) -> Result<StuffBundle, ()> {
    if spritesheet.atlas_handle().is_none() {
        return Err(());
    }
    Ok(StuffBundle {
        velocity: Velocity::linear(vel),
        sprite: SpriteSheetBundle {
            sprite: TextureAtlasSprite {
                flip_x: thread_rng().gen(),
                flip_y: thread_rng().gen(),
                index: thread_rng().gen_range(0..spritesheet.frames.len()),
                ..Default::default()
            },
            texture_atlas: spritesheet.atlas_handle().unwrap(),
            transform: Transform::from_translation(pos.extend(0.0)),
            ..Default::default()
        },
        collider: Collider::ball(6.0),
        // TODO: individual colliders, friction, restitution, damping
        ..Default::default()
    })
}

pub fn fill_stuff(
    point_a: Vec2,
    point_b: Vec2,
    interval: f32,
    spritesheet: &Spritesheet,
) -> Vec<StuffBundle> {
    let mut vec = Vec::<StuffBundle>::new();

    let min = point_a.min(point_b);
    let max = point_a.max(point_b);

    let width = ((max.x - min.x).abs() / interval).floor() as u32;
    let height = ((max.y - min.y).abs() / interval).floor() as u32;
    for x in 0..width {
        for y in 0..height {
            let pos = Vec2::new(
                min.x + (x as f32 * interval),
                min.y + (y as f32 * interval),
            );
            vec.push(spawn_stuff(pos, Vec2::ZERO, spritesheet).unwrap())
        }
    }

    vec
}
