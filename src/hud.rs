use bevy::prelude::*;

use crate::item::ItemsCollected;

// -------------------------------------------------------------------------------------------------

pub struct HudPlugin;

#[derive(Component)]
struct CoinCounter;

// -------------------------------------------------------------------------------------------------

impl Plugin for HudPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, setup_hud)
            .add_systems(Update, handle_hud);
    }
}

// -------------------------------------------------------------------------------------------------

fn setup_hud(mut commands: Commands) {
    commands.spawn((
        CoinCounter,
        TextBundle {
            text: Text {
                sections: vec![
                    TextSection {
                        value: "Coins: ".to_string(),
                        style: TextStyle {
                            color: Color::GOLD,
                            font_size: 30.0,
                            ..Default::default()
                        },
                    },
                    TextSection {
                        value: "0".to_string(),
                        style: TextStyle {
                            color: Color::GOLD,
                            font_size: 30.0,
                            ..Default::default()
                        },
                    },
                ],
                ..Default::default()
            },
            ..Default::default()
        },
    ));
}

fn handle_hud(
    collected_items: Res<ItemsCollected>,
    mut coin_counter_query: Query<&mut Text, With<CoinCounter>>,
) {
    if let Ok(mut coin_count) = coin_counter_query.get_single_mut() {
        coin_count.sections[1].value =
            format!("{} / {}", collected_items.coins, collected_items.max_coins).to_string();
    }
}
