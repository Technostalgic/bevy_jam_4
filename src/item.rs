use bevy::{prelude::*, sprite::Anchor};
use bevy_asepritesheet::prelude::*;
use bevy_rapier2d::{prelude::*, rapier::geometry::CollisionEventFlags};

use crate::{
    hero::Hero,
    mouse::MouseController,
    tooltips::{spawn_tooltip, TooltipCondition},
};

// -------------------------------------------------------------------------------------------------

pub struct ItemPlugin;

#[derive(Component, Default, Clone, Copy, PartialEq, Eq)]
pub enum Item {
    #[default]
    Coin,
    MagicHand,
    Snowflake,
    _Win,
}

#[derive(Component)]
pub struct ItemGfxNeeded;

#[derive(Bundle)]
pub struct ItemBundle {
    item: Item,
    collider: Collider,
    sensor: Sensor,
    animated_sprite: AnimatedSpriteBundle,
    atlas_needed: ItemGfxNeeded,
}

#[derive(Resource, Default)]
pub struct ItemsCollected {
    pub coins: u32,
    pub max_coins: u32,
    pub magic_hand: bool,
    pub snowflake: bool,
    pub win: bool,
}

#[derive(Resource, Default)]
pub struct ItemAssetData {
    is_loading: bool,
    loading_finished: bool,
    coin_sheet_handle: Handle<SpritesheetData>,
    coin_sheet: Option<Spritesheet>,
    hand_sheet_handle: Handle<SpritesheetData>,
    hand_sheet: Option<Spritesheet>,
    snowflake_sheet_handle: Handle<SpritesheetData>,
    snowflake_sheet: Option<Spritesheet>,
}

// -------------------------------------------------------------------------------------------------

impl Plugin for ItemPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(ItemsCollected::default())
            .insert_resource(ItemAssetData::default())
            .add_systems(Update, (load, handle_item_collision))
            .add_systems(PostUpdate, handle_insert_gfx);
    }
}

impl Default for ItemBundle {
    fn default() -> Self {
        Self {
            item: default(),
            collider: Collider::ball(8.0),
            sensor: Sensor,
            animated_sprite: default(),
            atlas_needed: ItemGfxNeeded,
        }
    }
}

// -------------------------------------------------------------------------------------------------

fn load(
    mut load_data: ResMut<ItemAssetData>,
    sprite_assets: Res<Assets<SpritesheetData>>,
    mut atlas_assets: ResMut<Assets<TextureAtlas>>,
    asset_server: Res<AssetServer>,
) {
    if !load_data.is_loading {
        load_data.is_loading = true;
        load_data.coin_sheet_handle = asset_server.load::<SpritesheetData>("coin.sprite.json");
        load_data.hand_sheet_handle = asset_server.load::<SpritesheetData>("hand.sprite.json");
        load_data.snowflake_sheet_handle =
            asset_server.load::<SpritesheetData>("snowflake.sprite.json");
    } else if !load_data.loading_finished {
        if load_data.coin_sheet.is_none() {
            if let Some(sprite_data) = sprite_assets.get(load_data.coin_sheet_handle.clone()) {
                load_data.coin_sheet = Some(Spritesheet::from_data(
                    sprite_data,
                    &asset_server,
                    Anchor::Center,
                    &mut atlas_assets,
                ));
            }
        }
        if load_data.hand_sheet.is_none() {
            if let Some(sprite_data) = sprite_assets.get(load_data.hand_sheet_handle.clone()) {
                load_data.hand_sheet = Some(Spritesheet::from_data(
                    sprite_data,
                    &asset_server,
                    Anchor::Center,
                    &mut atlas_assets,
                ));
            }
        }
        if load_data.snowflake_sheet.is_none() {
            if let Some(sprite_data) = sprite_assets.get(load_data.snowflake_sheet_handle.clone()) {
                load_data.snowflake_sheet = Some(Spritesheet::from_data(
                    sprite_data,
                    &asset_server,
                    Anchor::Center,
                    &mut atlas_assets,
                ));
            }
        }
        if load_data.coin_sheet.is_some()
            && load_data.hand_sheet.is_some()
            && load_data.snowflake_sheet.is_some()
        {
            load_data.loading_finished = true;
        }
    }
}

fn handle_insert_gfx(
    mut commands: Commands,
    asset_data: Res<ItemAssetData>,
    mut query: Query<
        (
            Entity,
            &Item,
            &mut SpriteAnimator,
            &mut Handle<TextureAtlas>,
        ),
        With<ItemGfxNeeded>,
    >,
) {
    for (ent, itm, mut spr_anim, mut tex_handle) in &mut query {
        let targ = match itm {
            Item::Coin => {
                if let Some(sheet) = asset_data.coin_sheet.as_ref() {
                    Some((sheet.clone(), sheet.atlas_handle().unwrap()))
                } else {
                    None
                }
            }
            Item::MagicHand => {
                if let Some(sheet) = asset_data.hand_sheet.as_ref() {
                    Some((sheet.clone(), sheet.atlas_handle().unwrap()))
                } else {
                    None
                }
            }
            Item::Snowflake => {
                if let Some(sheet) = asset_data.snowflake_sheet.as_ref() {
                    Some((sheet.clone(), sheet.atlas_handle().unwrap()))
                } else {
                    None
                }
            }
            _ => None,
        };
        if let Some((sheet, handle)) = targ {
            spr_anim.set_spritesheet(sheet);
            *tex_handle = handle;
            commands.entity(ent).remove::<ItemGfxNeeded>();
        }
    }
}

fn handle_item_collision(
    mut commands: Commands,
    mut col_evts: EventReader<CollisionEvent>,
    mut collected_items: ResMut<ItemsCollected>,
    mut mouse: ResMut<MouseController>,
    hero_query: Query<(), With<Hero>>,
    item_query: Query<(Entity, &Item)>,
) {
    for evt in col_evts.read() {
        if let CollisionEvent::Started(ent1, ent2, flags) = evt {
            if flags.contains(CollisionEventFlags::SENSOR) {
                if hero_query.get(*ent1).is_ok() || hero_query.get(*ent2).is_ok() {
                    if let Ok((ent, itm)) = item_query.get(*ent1).or(item_query.get(*ent2)) {
                        commands.entity(ent).despawn();
                        match itm {
                            Item::Coin => {
                                collected_items.coins += 1;
                                if collected_items.coins >= collected_items.max_coins {
                                    commands.spawn(spawn_tooltip(
                                        "You Win! I was too lazy to make a win screen so this is all you get"
                                        .to_string(),
                                        TooltipCondition::Time(5.0),
                                    ));
                                }
                            }
                            Item::MagicHand => {
                                collected_items.magic_hand = true;
                                mouse.active = true;
                                commands.spawn(spawn_tooltip(
                                    "Magic Hand! Use Mouse Click to grab and move objects"
                                        .to_string(),
                                    TooltipCondition::Grab,
                                ));
                            }
                            Item::Snowflake => {
                                collected_items.snowflake = true;
                                mouse.freeze_ability = true;
                                commands.spawn(spawn_tooltip(
                                    "Snow Flake! Right click while holding objects to freeze them"
                                        .to_string(),
                                    TooltipCondition::Freeze,
                                ));
                            }
                            Item::_Win => {
                                collected_items.win = true;
                            }
                        }
                    }
                }
            }
        }
    }
}

// -------------------------------------------------------------------------------------------------

pub fn spawn_item_coin(world_pos: Vec2) -> ItemBundle {
    ItemBundle {
        item: Item::Coin,
        animated_sprite: AnimatedSpriteBundle {
            sprite: SpriteSheetBundle {
                transform: Transform::from_translation(world_pos.extend(-10.0)),
                ..Default::default()
            },
            ..Default::default()
        },
        ..Default::default()
    }
}

pub fn spawn_item_hand(world_pos: Vec2) -> ItemBundle {
    ItemBundle {
        item: Item::MagicHand,
        animated_sprite: AnimatedSpriteBundle {
            sprite: SpriteSheetBundle {
                transform: Transform::from_translation(world_pos.extend(0.0)),
                ..Default::default()
            },
            ..Default::default()
        },
        ..Default::default()
    }
}

pub fn spawn_item_snowflake(world_pos: Vec2) -> ItemBundle {
    ItemBundle {
        item: Item::Snowflake,
        animated_sprite: AnimatedSpriteBundle {
            sprite: SpriteSheetBundle {
                transform: Transform::from_translation(world_pos.extend(0.0)),
                ..Default::default()
            },
            ..Default::default()
        },
        ..Default::default()
    }
}
