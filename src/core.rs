use std::time::Duration;

use bevy::{
    core_pipeline::clear_color::ClearColorConfig,
    ecs::system::SystemId,
    prelude::*,
    render::{
        camera::RenderTarget,
        render_resource::{
            Extent3d, TextureDescriptor, TextureDimension, TextureFormat, TextureUsages,
        },
        view::RenderLayers,
    },
    time::Stopwatch,
    window::{PrimaryWindow, WindowResized},
};
use bevy_rapier2d::prelude::*;

use crate::{
    hero::{Hero, HeroPlugin},
    hud::HudPlugin,
    item::{Item, ItemPlugin, ItemsCollected},
    level::{LevelPlugin, LoadingLevel},
    mouse::{MouseController, MousePlugin},
    stuff::{StuffPlugin, spawn_stuff, StuffData, Stuff},
    tooltips::TooltipPlugin,
};

// -------------------------------------------------------------------------------------------------

pub const UPSCALE_LAYER: RenderLayers = RenderLayers::layer(1);

pub struct CorePlugin;

#[derive(Resource)]
pub struct GameTime {
    pub time_scale: f32,
    pub paused: bool,
    timer: Stopwatch,
    cur_time_elapsed: Duration,
    last_time_elapsed: Duration,
}

#[derive(Component)]
pub struct WorldCam;

#[derive(Component)]
pub struct UpscaleCam;

#[derive(Resource, Default)]
pub struct RenderUpscaling {
    pub back_buff_size: Vec2,
    pub upscale_factor: f32,
}

#[derive(Resource)]
pub struct CoreSystemIds {
    pub recalc_upscale: SystemId,
}

// -------------------------------------------------------------------------------------------------

impl Plugin for CorePlugin {
    fn build(&self, app: &mut App) {
        let sys_ids = CoreSystemIds {
            recalc_upscale: app.world.register_system(recalc_upscale),
        };
        app.add_plugins((
            HeroPlugin,
            StuffPlugin,
            ItemPlugin,
            MousePlugin,
            LevelPlugin,
            TooltipPlugin,
            HudPlugin,
        ))
        .insert_resource(sys_ids)
        .insert_resource(GameTime::default())
        .insert_resource(RenderUpscaling::default())
        .add_systems(Startup, (setup_camera, setup_config))
        .add_systems(PreUpdate, handle_game_time)
        .add_systems(
            Update,
            (
                handle_debug,
                recalc_upscale_on_resize,
                on_level_loaded.run_if(resource_removed::<LoadingLevel>()),
            ),
        )
        .add_systems(PostUpdate, handle_camera);
    }
}

impl Default for GameTime {
    fn default() -> Self {
        Self {
            time_scale: 1.0,
            paused: true,
            timer: default(),
            cur_time_elapsed: default(),
            last_time_elapsed: default(),
        }
    }
}

impl GameTime {
    /// get the total elapsed game time, in seconds
    pub fn _elapsed_secs(&self) -> f32 {
        self.cur_time_elapsed.as_secs_f32()
    }
    /// get the total elapsed game time duration
    pub fn _elapsed(&self) -> Duration {
        self.cur_time_elapsed
    }
    /// get the delta duration since last tick
    pub fn delta(&self) -> Duration {
        self.cur_time_elapsed - self.last_time_elapsed
    }
    /// get the elapsed game since the last step, in seconds
    pub fn delta_secs(&self) -> f32 {
        (self.cur_time_elapsed - self.last_time_elapsed).as_secs_f32()
    }
    /// starts running the game time if it's not already running
    pub fn start(&mut self) -> &mut Self {
        self.paused = false;
        self
    }
    /// stops running the game time until start is called
    pub fn pause(&mut self) -> &mut Self {
        self.paused = true;
        self
    }
    /// reset the game time back to 0
    pub fn _reset(&mut self) -> &mut Self {
        self.timer.reset();
        self
    }
}

// Systems: ----------------------------------------------------------------------------------------

fn on_level_loaded(
    mut game_time: ResMut<GameTime>,
    mut rapier_config: ResMut<RapierConfiguration>,
    mut collected_items: ResMut<ItemsCollected>,
    item_query: Query<&Item>,
) {
    game_time.start();
    rapier_config.physics_pipeline_active = true;
    for itm in &item_query {
        match itm {
            Item::Coin => {
                collected_items.max_coins += 1;
            }
            _ => {}
        }
    }
    println!("Game Started!");
}

fn handle_game_time(time: Res<Time>, mut game_time: ResMut<GameTime>) {
    if !game_time.paused {
        let time_scale = game_time.time_scale;
        game_time.timer.tick(Duration::from_secs_f32(
            time.delta().as_secs_f32() * time_scale,
        ));
    }
    let now = game_time.timer.elapsed();
    game_time.last_time_elapsed = game_time.cur_time_elapsed;
    game_time.cur_time_elapsed = now;
}

fn setup_camera(
    mut commands: Commands,
    core_sys_ids: Res<CoreSystemIds>,
    mut upscale: ResMut<RenderUpscaling>,
    mut image_assets: ResMut<Assets<Image>>,
) {
    // create render target for main world cam
    let back_buff_size = Extent3d {
        width: 400,
        height: 300,
        ..Default::default()
    };
    let mut back_buff = Image {
        texture_descriptor: TextureDescriptor {
            label: None,
            dimension: TextureDimension::D2,
            format: TextureFormat::Rgba8UnormSrgb,
            mip_level_count: 1,
            sample_count: 1,
            size: back_buff_size,
            usage: TextureUsages::RENDER_ATTACHMENT
                | TextureUsages::TEXTURE_BINDING
                | TextureUsages::COPY_DST,
            view_formats: &[],
        },
        ..Default::default()
    };
    upscale.back_buff_size = Vec2::new(back_buff_size.width as f32, back_buff_size.height as f32);
    back_buff.resize(back_buff_size);
    let back_buff_handle = image_assets.add(back_buff);

    // spawn the main world cam
    commands.spawn((
        WorldCam,
        UiCameraConfig { show_ui: false },
        Camera2dBundle {
            camera_2d: Camera2d {
                clear_color: ClearColorConfig::Custom(Color::rgb(0.135, 0.1, 0.3)),
            },
            projection: OrthographicProjection {
                scale: 1.0,
                near: -1000.0,
                far: 1000.0,
                ..Default::default()
            },
            camera: Camera {
                target: RenderTarget::Image(back_buff_handle.clone()),
                ..Default::default()
            },
            ..Default::default()
        },
    ));

    commands.spawn((
        UPSCALE_LAYER,
        SpriteBundle {
            texture: back_buff_handle,
            ..Default::default()
        },
    ));

    // spawn cam to render back buffer to screen
    commands.spawn((
        UPSCALE_LAYER,
        UpscaleCam,
        Camera2dBundle {
            camera_2d: Camera2d {
                clear_color: ClearColorConfig::Custom(Color::BLACK),
            },
            projection: OrthographicProjection {
                scale: 1.0,
                near: -1000.0,
                far: 1000.0,
                ..Default::default()
            },
            ..Default::default()
        },
    ));

    commands.run_system(core_sys_ids.recalc_upscale);
}

fn handle_camera(
    game_time: Res<GameTime>,
    hero_query: Query<(&GlobalTransform, &Velocity), With<Hero>>,
    mut cam_query: Query<&mut Transform, With<WorldCam>>,
) {
    let mut cam_trans = cam_query.single_mut();
    let mut target_pos = Vec2::default();
    let vel_weight = 0.35;
    let mut hero_count: usize = 0;
    for (glob_trans, vel) in &hero_query {
        hero_count += 1;
        let hero_targ = glob_trans.translation().truncate() + vel.linvel * vel_weight;
        target_pos += hero_targ;
    }
    if hero_count <= 0 {
        return;
    }

    let follow_weight = 2.0;
    let follow_weight = (follow_weight * game_time.delta_secs()).min(1.0);
    let target_pos = target_pos / (hero_count as f32);
    let cur_pos = cam_trans.translation.truncate();
    let final_pos = cur_pos * (1.0 - follow_weight) + target_pos * follow_weight;

    cam_trans.translation = final_pos.extend(0.0).round();
}

fn recalc_upscale(
    window_query: Query<&Window, With<PrimaryWindow>>,
    image_assets: ResMut<Assets<Image>>,
    cam_query: Query<&Camera, With<WorldCam>>,
    mut upscale: ResMut<RenderUpscaling>,
    mut upscale_proj_query: Query<&mut OrthographicProjection, With<UpscaleCam>>,
) {
    let window = window_query.single();
    let cam = cam_query.single();
    let mut upscl_proj = upscale_proj_query.single_mut();

    if let RenderTarget::Image(rdr_targ_handle) = &cam.target {
        if let Some(back_buff) = image_assets.get(rdr_targ_handle) {
            let upscale_x: f32 = (window.width() / back_buff.size_f32().x).floor();
            let upscale_y: f32 = (window.height() / back_buff.size_f32().y).floor();
            upscale.upscale_factor = upscale_x.min(upscale_y);
            upscl_proj.scale = 1.0 / upscale.upscale_factor;
        }
    }
}

fn recalc_upscale_on_resize(
    mut commands: Commands,
    core_sys_ids: Res<CoreSystemIds>,
    mut window_resize_evts: EventReader<WindowResized>,
) {
    let mut did_resize = false;
    for _ in window_resize_evts.read() {
        if !did_resize {
            commands.run_system(core_sys_ids.recalc_upscale);
            did_resize = true;
        }
    }
}

fn setup_config(
    mut rapier_debug_render: ResMut<DebugRenderContext>,
    mut rapier_config: ResMut<RapierConfiguration>,
    mut msaa: ResMut<Msaa>,
) {
    println!("Game PAUSED!");
    *msaa = Msaa::Off;
    rapier_debug_render.enabled = false;
    rapier_debug_render.pipeline.mode =
        DebugRenderMode::COLLIDER_SHAPES | DebugRenderMode::CONTACTS;
    rapier_config.timestep_mode = TimestepMode::Variable {
        max_dt: 1.0 / 50.0,
        time_scale: 1.0,
        substeps: 1,
    };
    rapier_config.physics_pipeline_active = false;
    rapier_config.gravity.y = -1200.0;
}

fn handle_debug(
    time: Res<Time>,
    mut commands: Commands,
    keys: Res<Input<KeyCode>>,
    mut rapier_debug_render: ResMut<DebugRenderContext>,
    mouse: Res<MouseController>,
    stuff_data: Res<StuffData>,
    stuff_query: Query<Entity, With<Stuff>>,
) {
    if keys.just_pressed(KeyCode::F1) {
        println!("Mouse position: {:?}", mouse.world_position.round());
    }
    if keys.just_pressed(KeyCode::F2) {
        rapier_debug_render.enabled = !rapier_debug_render.enabled;
    }
    if keys.just_pressed(KeyCode::F3) {
        for thing in &stuff_query {
            commands.entity(thing).despawn();
        }
    }
    if keys.pressed(KeyCode::F4) {
        if let Some(sheet) = &stuff_data.spritesheet {
            if let Ok(thing) = spawn_stuff(mouse.world_position, Vec2::ZERO, &sheet) {
                commands.spawn(thing);
            }
        }
    }

    if keys.pressed(KeyCode::Return) {
        let col_group = if time.elapsed_seconds() % 0.25 >= 0.125 {
            Group::from_bits_truncate(0b10)
        } else {
            Group::from_bits_truncate(0b01)
        };
        commands
            .spawn(RigidBody::Dynamic)
            .insert(Collider::ball(12.0))
            .insert(Velocity {
                linvel: Vec2::new(0.0, 0.0),
                angvel: 0.0,
            })
            .insert(Restitution::coefficient(0.5))
            .insert(TransformBundle::from(Transform::from_xyz(0.0, 300.0, 0.0)))
            .insert(CollisionGroups::new(col_group, col_group));
    }
}
