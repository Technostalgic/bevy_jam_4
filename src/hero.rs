use std::time::Duration;

use bevy::{prelude::*, sprite::Anchor, time::Stopwatch};
use bevy_asepritesheet::prelude::*;
use bevy_rapier2d::prelude::*;

use crate::core::GameTime;

// -------------------------------------------------------------------------------------------------

const CONTROL_JUMP: KeyCode = KeyCode::Space;
const CONTROL_ATTACK: KeyCode = KeyCode::X;
const CONTROL_ATTACK2: KeyCode = KeyCode::C;
const CONTROL_MOVE_RIGHT: KeyCode = KeyCode::D;
const CONTROL_MOVE_LEFT: KeyCode = KeyCode::A;
const CONTROL_MOVE_UP: KeyCode = KeyCode::W;
const CONTROL_MOVE_DOWN: KeyCode = KeyCode::S;

pub struct HeroPlugin;

#[derive(Resource, Default)]
pub struct HeroData {
    pub anim_idle: AnimHandle,
    pub anim_run: AnimHandle,
    pub anim_jump: AnimHandle,
    pub anim_fall: AnimHandle,
    pub anim_collect: AnimHandle,
    pub anim_attack: AnimHandle,
    is_loading: bool,
    spritesheet_handle: Handle<SpritesheetData>,
    spritesheet: Option<Spritesheet>,
}

#[derive(Component)]
pub struct Hero {
    pub flipped: bool,
    pub max_speed: f32,
    pub acceleration: f32,
    pub jump_power: f32,
    pub jump_sustain: f32,
    climbing: bool,
    surface_friction_coeff: f32,
    coyote_time: Stopwatch,
    max_coyote_time: f32,
    ground_normal_thresh: f32,
}

#[derive(Bundle)]
pub struct HeroBundle {
    pub hero: Hero,
    pub rigidbody: RigidBody,
    pub collider: Collider,
    pub active_evts: ActiveEvents,
    pub locked_axes: LockedAxes,
    pub velocity: Velocity,
    pub friction: Friction,
    pub animated_sprite: AnimatedSpriteBundle,
}

// -------------------------------------------------------------------------------------------------

impl Plugin for HeroPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(HeroData::default())
            .add_systems(PreUpdate, initialize_hero)
            .add_systems(
                Update,
                (
                    load,
                    spawn_hero,
                    (handle_hero_ground, handle_hero_input).chain(),
                ),
            );
    }
}

impl Default for HeroBundle {
    fn default() -> Self {
        Self {
            hero: Hero::default(),
            collider: Collider::capsule_y(8.0, 6.0),
            active_evts: ActiveEvents::COLLISION_EVENTS,
            rigidbody: RigidBody::Dynamic,
            locked_axes: LockedAxes::ROTATION_LOCKED,
            velocity: Velocity::default(),
            friction: Friction {
                coefficient: 0.5,
                combine_rule: CoefficientCombineRule::Min,
            },
            animated_sprite: AnimatedSpriteBundle {
                animator: SpriteAnimator::default(),
                sprite: SpriteSheetBundle::default(),
            },
        }
    }
}

impl Default for Hero {
    fn default() -> Self {
        Self {
            flipped: false,
            max_speed: 200.0,
            acceleration: 1500.0,
            jump_power: 300.0,
            jump_sustain: 0.5,
            climbing: false,
            surface_friction_coeff: 0.0,
            coyote_time: default(),
            max_coyote_time: 0.2,
            ground_normal_thresh: 0.8,
        }
    }
}

impl Hero {
    pub fn on_ground(&self) -> bool {
        self.coyote_time.elapsed_secs() <= self.max_coyote_time
    }
}

// Systems: ----------------------------------------------------------------------------------------

fn load(
    asset_server: Res<AssetServer>,
    spritesheet_assets: Res<Assets<SpritesheetData>>,
    mut atlas_assets: ResMut<Assets<TextureAtlas>>,
    mut hero_data: ResMut<HeroData>,
) {
    if !hero_data.is_loading && hero_data.spritesheet.is_none() {
        println!("Loading Hero GFX");
        hero_data.is_loading = true;
        hero_data.spritesheet_handle = asset_server.load("hero.sprite.json");
    } else if hero_data.spritesheet.is_none() {
        if let Some(sheet_data) = spritesheet_assets.get(&hero_data.spritesheet_handle) {
            let mut sheet = Spritesheet::from_data(
                &sheet_data,
                &asset_server,
                Anchor::Center,
                &mut atlas_assets,
            );
            format_hero_anims(&mut sheet, &mut hero_data);
            hero_data.spritesheet = Some(sheet);
            hero_data.is_loading = false;
            println!("Hero GFX Loaded!");
        }
    }
}

fn spawn_hero(mut commands: Commands, hero_data: Res<HeroData>, hero_query: Query<&Hero>) {
    // don't spawn player until loading finished
    if hero_data.spritesheet.is_none() {
        return;
    }

    // don't spawn player if there already is one
    for _ in &hero_query {
        return;
    }

    // spawn player
    commands.spawn(HeroBundle::default());
}

fn initialize_hero(
    hero_data: Res<HeroData>,
    mut query: Query<
        (
            &mut Hero,
            &mut Friction,
            &mut SpriteAnimator,
            &mut Handle<TextureAtlas>,
        ),
        Added<Hero>,
    >,
) {
    for (mut hero, mut hero_friction, mut spr_anim, mut atlas_handle) in &mut query {
        hero.surface_friction_coeff = hero_friction.coefficient;
        hero_friction.combine_rule = CoefficientCombineRule::Min;
        let sheet = hero_data.spritesheet.as_ref().unwrap();
        spr_anim.set_spritesheet(sheet.clone());
        *atlas_handle = sheet.atlas_handle().unwrap();
        spr_anim.set_anim(hero_data.anim_idle).unwrap();
    }
}

fn handle_hero_input(
    time: Res<GameTime>,
    keys: Res<Input<KeyCode>>,
    rapier_config: Res<RapierConfiguration>,
    hero_data: Res<HeroData>,
    mut hero_query: Query<(
        &mut Hero,
        &mut Velocity,
        &mut Friction,
        &mut TextureAtlasSprite,
        &mut SpriteAnimator,
    )>,
) {
    let jumping = keys.just_pressed(CONTROL_JUMP);
    let jump_sustaining = keys.pressed(CONTROL_JUMP);
    let attacking = keys.just_pressed(CONTROL_ATTACK) || keys.just_pressed(CONTROL_ATTACK2);
    let mut move_x: f32 = 0.0;
    let mut move_y: f32 = 0.0;
    let dt = time.delta_secs();

    if keys.pressed(CONTROL_MOVE_LEFT) {
        move_x -= 1.0;
    }
    if keys.pressed(CONTROL_MOVE_RIGHT) {
        move_x += 1.0;
    }
    if keys.pressed(CONTROL_MOVE_UP) {
        move_y += 1.0;
    }
    if keys.pressed(CONTROL_MOVE_DOWN) {
        move_y -= 1.0;
    }

    for (mut hero, mut hero_vel, mut hero_fric, mut spr, mut spr_anim) in &mut hero_query {
        handle_hero_movement(dt, move_x, move_y, &hero, &mut hero_vel, &mut hero_fric);

        // handle horizontal flip
        if move_x >= 0.1 {
            hero.flipped = false;
            spr.flip_x = false;
        } else if move_x <= -0.1 {
            hero.flipped = true;
            spr.flip_x = true;
        }

        // running
        if hero.on_ground() {
            if move_x.abs() < 0.1 {
                if !spr_anim.is_cur_anim(hero_data.anim_idle) {
                    spr_anim.set_anim(hero_data.anim_idle).unwrap();
                }
            } else {
                if !spr_anim.is_cur_anim(hero_data.anim_run) {
                    spr_anim.set_anim(hero_data.anim_run).unwrap();
                }
            }
            if jumping {
                if !spr_anim.is_cur_anim(hero_data.anim_jump) {
                    spr_anim.set_anim(hero_data.anim_jump).unwrap();
                }
                hero_vel.linvel.y = hero.jump_power;
                let max_coyote_time = hero.max_coyote_time;
                hero.coyote_time
                    .set_elapsed(Duration::from_secs_f32(max_coyote_time + 0.01));
            }

        // sustaining jump
        } else if jump_sustaining && !jumping && hero_vel.linvel.y > 0.0 {
            if !spr_anim.is_cur_anim(hero_data.anim_jump) {
                spr_anim.set_anim(hero_data.anim_jump).unwrap();
            }
            hero_vel.linvel.y += -rapier_config.gravity.y * hero.jump_sustain * dt;

        // falling
        } else {
            if !spr_anim.is_cur_anim(hero_data.anim_fall) {
                spr_anim.set_anim(hero_data.anim_fall).unwrap();
            }
        }
        if attacking {
            println!("ATTACKING")
        }
    }
}

fn handle_hero_ground(
    game_time: Res<GameTime>,
    phys: Res<RapierContext>,
    mut cont_evts: EventReader<ContactForceEvent>,
    mut hero_query: Query<(Entity, &mut Hero)>,
) {
    // return if paused
    if game_time.delta_secs() <= 0.0 {
        return;
    }

    // for each hero
    for (hero_ent, mut hero) in &mut hero_query {
        // increment the coyote time for each hero
        hero.coyote_time.tick(game_time.delta());

        // used to calc average normal between all contacts
        let mut av_norm = Vec2::default();
        let mut av_weight: usize = 0;

        // check each contact point on the hero to see if it is a contact with the ground
        'outer: for contact in phys.contacts_with(hero_ent) {
            if !contact.has_any_active_contacts() {
                continue;
            }
            let is_first_shape = hero_ent == contact.collider1();
            for man in contact.manifolds() {
                let norm = if is_first_shape {
                    man.normal() * -1.0
                } else {
                    man.normal()
                };
                av_weight += 1;
                av_norm += norm;
                // check to see if the contact normal is from a shallow enough slope to be ground
                if norm.y >= hero.ground_normal_thresh {
                    hero.coyote_time.reset();
                    break 'outer;
                }
            }
        }

        // calculate average of all contact normals and normalize result, which can also be used
        // to detect ground. this is in the case that the hero is stuck or wedged between two steep
        // slopes, so that it counts as a 'ground' and the player doesn't get stuck in an inifinite
        // falling loop. However, we don't want to only rely on this average because contact normals
        // from above going into the average can count "against" the ground slope
        av_norm /= av_weight as f32;
        if av_norm.normalize().y >= hero.ground_normal_thresh {
            hero.coyote_time.reset();
        }
    }

    // check for ground contacts in contact events
    for evt in cont_evts.read() {
        // see if the event includes a hero entity
        let hero = if let Ok(val) = hero_query.get_mut(evt.collider1) {
            Some(val.1)
        } else if let Ok(val) = hero_query.get_mut(evt.collider2) {
            Some(val.1)
        } else {
            None
        };

        if let Some(mut hero) = hero {
            // check force slope to see if it's on a shallow enough slope to consider it ground
            let force_dir = evt.total_force.normalize();
            if force_dir.y > hero.ground_normal_thresh {
                // if on ground, reset the coyote time, essentially marking the hero as on ground
                hero.coyote_time.reset();
            }
        }
    }
}

// Utility: ----------------------------------------------------------------------------------------

fn format_hero_anims(sheet: &mut Spritesheet, data: &mut HeroData) {
    // get handles to all animations
    data.anim_idle = sheet.get_anim_handle("idle").unwrap();
    data.anim_run = sheet.get_anim_handle("run").unwrap();
    data.anim_jump = sheet.get_anim_handle("jump").unwrap();
    data.anim_fall = sheet.get_anim_handle("fall").unwrap();
    data.anim_collect = sheet.get_anim_handle("collect").unwrap();
    data.anim_attack = sheet.get_anim_handle("attack").unwrap();
}

fn handle_hero_movement(
    delta_time: f32,
    move_x: f32,
    move_y: f32,
    hero: impl AsRef<Hero>,
    hero_vel: &mut Velocity,
    hero_fric: &mut Friction,
) {
    let hero = hero.as_ref();
    let targ_vel_x = hero.max_speed * move_x.clamp(-1.0, 1.0);
    if move_x.abs() > 0.0 {
        hero_fric.coefficient = 0.0;
    } else {
        hero_fric.coefficient = hero.surface_friction_coeff;
    }

    // enforce max speed
    if hero_vel.linvel.x.abs() > targ_vel_x.abs() {
        let dif_vel_x = targ_vel_x - hero_vel.linvel.x;
        let acc_x = hero.acceleration * dif_vel_x.signum() * delta_time;
        if hero.on_ground() {
            if hero_vel.linvel.x.signum() != (hero_vel.linvel.x + acc_x).signum() {
                hero_vel.linvel.x = 0.0;
            } else {
                hero_vel.linvel.x += acc_x;
            }
        }
        hero_fric.coefficient = hero.surface_friction_coeff;

    // move
    } else {
        if hero_vel.linvel.x != 0.0
            && targ_vel_x != 0.0
            && hero_vel.linvel.x.signum() != targ_vel_x.signum()
        {
            hero_fric.coefficient = hero.surface_friction_coeff;
        }
        let acc_x = hero.acceleration * move_x * delta_time;
        if (hero_vel.linvel.x + acc_x).abs() >= targ_vel_x.abs() {
            hero_vel.linvel.x = targ_vel_x;
        } else {
            hero_vel.linvel.x += acc_x;
        }
    }

    if hero.climbing {
        let acc_y = hero.acceleration * move_y * delta_time;
        hero_vel.linvel.y += acc_y;
    }
}
