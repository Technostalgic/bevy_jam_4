use bevy::{prelude::*, time::Stopwatch};

use crate::{core::GameTime, hero::Hero, mouse::MouseController};

// -------------------------------------------------------------------------------------------------

pub struct TooltipPlugin;

#[derive(Component)]
pub struct Tooltip {
    pub condition: TooltipCondition,
    stopwatch: Stopwatch,
}

#[derive(Bundle)]
pub struct TooltipBundle {
    tooltip: Tooltip,
    text_bundle: Text2dBundle,
}

pub enum TooltipCondition {
    Time(f32),
    Flipped,
    Grab,
    Freeze,
}

// -------------------------------------------------------------------------------------------------

impl Plugin for TooltipPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, handle_tooltips);
    }
}

// -------------------------------------------------------------------------------------------------

fn handle_tooltips(
    mut commands: Commands,
    mouse: Res<MouseController>,
    game_time: Res<GameTime>,
    mut tooltip_query: Query<(Entity, &mut Transform, &mut Tooltip)>,
    hero_query: Query<(&GlobalTransform, &Hero)>,
) {
    for (ent, mut trans, mut tip) in &mut tooltip_query {
        for (hero_trans, _) in &hero_query {
            trans.translation.x = hero_trans.translation().x;
            trans.translation.y = hero_trans.translation().y + 25.0;
        }
        tip.stopwatch.tick(game_time.delta());
        match tip.condition {
            TooltipCondition::Time(secs) => {
                if tip.stopwatch.elapsed_secs() >= secs {
                    commands.entity(ent).despawn();
                }
            }
            TooltipCondition::Flipped => {
                for (_, hero) in &hero_query {
                    if hero.flipped {
                        commands.entity(ent).despawn();
                    }
                }
            }
            TooltipCondition::Grab => {
                if mouse.held_ents.len() > 0 {
                    commands.entity(ent).despawn();
                }
            }
            TooltipCondition::Freeze => {
                if mouse.is_holding && mouse.just_pressed_alt {
                    commands.entity(ent).despawn();
                }
            }
        }
    }
}

// -------------------------------------------------------------------------------------------------

pub fn spawn_tooltip(tip: String, end_condition: TooltipCondition) -> TooltipBundle {
    TooltipBundle {
        tooltip: Tooltip {
            condition: end_condition,
            stopwatch: default(),
        },
        text_bundle: Text2dBundle {
            text: Text {
                sections: vec![TextSection::new(tip, TextStyle::default())],
                ..Default::default()
            },
            transform: Transform::from_translation(Vec3::new(0.0, 0.0, 30.0)),
            ..Default::default()
        },
    }
}
