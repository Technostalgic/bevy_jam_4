use bevy::{input::InputSystem, prelude::*, sprite::Anchor, utils::HashSet, window::PrimaryWindow};
use bevy_asepritesheet::prelude::*;
use bevy_rapier2d::{
    dynamics::RigidBody,
    na::{Isometry2, Vector2},
    parry::shape::Ball,
    plugin::RapierContext,
    rapier::pipeline::QueryFilter,
};

use crate::{
    core::{RenderUpscaling, WorldCam},
    hero::Hero,
    stuff::Stuff,
};

// -------------------------------------------------------------------------------------------------

pub const CONTROL_GRAB: MouseButton = MouseButton::Left;
pub const CONTROL_FREEZE: MouseButton = MouseButton::Right;

pub struct MousePlugin;

#[derive(Resource)]
pub struct MouseController {
    pub active: bool,
    pub freeze_ability: bool,
    pub world_position: Vec2,
    pub world_delta: Vec2,
    pub is_holding: bool,
    pub just_pressed_alt: bool,
    grab_radius: f32,
    just_held: bool,
    just_released: bool,
    pub held_ents: HashSet<Entity>,
}

#[derive(Component)]
struct MouseGfx;

#[derive(Resource, Default)]
struct Loading(Option<Handle<SpritesheetData>>);

// -------------------------------------------------------------------------------------------------

impl Plugin for MousePlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(MouseController::default())
            .insert_resource(Loading::default())
            .add_systems(PreUpdate, handle_mouse_input.after(InputSystem))
            .add_systems(
                Update,
                (
                    load.run_if(resource_exists::<Loading>()),
                    handle_mouse_interaction,
                ),
            );
    }
}

impl Default for MouseController {
    fn default() -> Self {
        Self {
            active: default(),
            freeze_ability: default(),
            world_position: default(),
            world_delta: default(),
            is_holding: default(),
            just_pressed_alt: default(),
            grab_radius: 1.0,
            just_released: default(),
            just_held: default(),
            held_ents: default(),
        }
    }
}

// -------------------------------------------------------------------------------------------------

fn load(
    mut commands: Commands,
    mut load_data: ResMut<Loading>,
    asset_server: Res<AssetServer>,
    sprite_assets: Res<Assets<SpritesheetData>>,
    mut atlas_assets: ResMut<Assets<TextureAtlas>>,
) {
    if load_data.0.is_none() {
        println!("Loading Hand GFX");
        load_data.0 = Some(asset_server.load::<SpritesheetData>("hand.sprite.json"));
    } else if let Some(handle) = &load_data.0 {
        if let Some(sprite_data) = sprite_assets.get(handle) {
            println!("Hand GFX Loaded!");
            let sheet = Spritesheet::from_data(
                sprite_data,
                &asset_server,
                Anchor::Center,
                &mut atlas_assets,
            );
            commands.spawn((
                MouseGfx,
                AnimatedSpriteBundle {
                    sprite: SpriteSheetBundle {
                        texture_atlas: sheet.atlas_handle().as_ref().unwrap().clone(),
                        ..Default::default()
                    },
                    animator: SpriteAnimator::from_sheet(sheet),
                },
            ));
            commands.remove_resource::<Loading>();
        }
    }
}

fn handle_mouse_input(
    mut mouse: ResMut<MouseController>,
    mouse_input: Res<Input<MouseButton>>,
    upscale: Res<RenderUpscaling>,
    phys: Res<RapierContext>,
    cam_query: Query<(&GlobalTransform, &Camera), With<WorldCam>>,
    hero_query: Query<&GlobalTransform, (With<Hero>, Without<WorldCam>)>,
    mut hand_query: Query<&mut TextureAtlasSprite, With<MouseGfx>>,
    mut window_query: Query<&mut Window, With<PrimaryWindow>>,
) {
    if !mouse.active {
        return;
    }
    let (cam_trans, cam) = cam_query.single();
    let mut window = window_query.single_mut();
    let last_world_pos = mouse.world_position;
    if let Some(mut cursor) = window.cursor_position() {
        window.cursor.visible = false;
        cursor /= upscale.upscale_factor;
        let off = (Vec2::new(window.width() as f32, window.height() as f32)
            - upscale.back_buff_size * upscale.upscale_factor)
            * -0.5
            / upscale.upscale_factor;
        cursor += off;
        if let Some(world_pos) = cam.viewport_to_world_2d(cam_trans, cursor) {
            mouse.world_position = world_pos;
        }
    }
    mouse.is_holding = mouse_input.pressed(CONTROL_GRAB);
    mouse.just_pressed_alt = mouse_input.just_pressed(CONTROL_FREEZE);
    mouse.just_held = mouse_input.just_pressed(CONTROL_GRAB);
    mouse.just_released = mouse_input.just_released(CONTROL_GRAB);
    mouse.world_delta = mouse.world_position - last_world_pos;
    let mut close_to_hero = false;
    for hero_trans in &hero_query {
        let dist = hero_trans
            .translation()
            .truncate()
            .distance(mouse.world_position);
        let hero_radius = 1.0;
        if dist <= (mouse.grab_radius + hero_radius) * phys.physics_scale() {
            close_to_hero = true;
            break;
        }
    }
    if close_to_hero {
        if let Ok(mut hand_spr) = hand_query.get_single_mut() {
            hand_spr.color = Color::rgba(1.0, 1.0, 1.0, 0.15);
        }
        if mouse.is_holding || mouse.just_held {
            mouse.just_released = true;
        }
        mouse.is_holding = false;
        mouse.just_held = false;
    } else if let Ok(mut hand_spr) = hand_query.get_single_mut() {
        hand_spr.color = Color::WHITE;
    }
}

fn handle_mouse_interaction(
    phys: Res<RapierContext>,
    mut mouse: ResMut<MouseController>,
    mut hand_query: Query<
        (&mut Transform, &mut SpriteAnimator, &mut Visibility),
        (With<MouseGfx>, Without<Stuff>),
    >,
    mut stuff_query: Query<(&mut Transform, &mut RigidBody, &mut TextureAtlasSprite), With<Stuff>>,
) {
    if !mouse.active {
        if let Ok((_, _, mut vis)) = hand_query.get_single_mut() {
            *vis = Visibility::Hidden;
        }
        return;
    }
    if let Ok((mut hand_trans, mut hand_anim, mut vis)) = hand_query.get_single_mut() {
        *vis = Visibility::Visible;
        hand_trans.translation = mouse.world_position.extend(15.0).round();

        if mouse.is_holding {
            if let Err(err) = hand_anim.set_anim_index(1) {
                warn!("{:?}", err);
            }
        } else {
            if let Err(err) = hand_anim.set_anim_index(0) {
                warn!("{:?}", err);
            }
        }

        // grab stuff
        if mouse.just_held {
            mouse.held_ents.clear();
            let shape = Ball::new(mouse.grab_radius);
            let shape_pos = Isometry2::new(
                Vector2::new(
                    mouse.world_position.x / phys.physics_scale(),
                    mouse.world_position.y / phys.physics_scale(),
                ),
                0.0,
            );
            phys.query_pipeline.intersections_with_shape(
                &phys.bodies,
                &phys.colliders,
                &shape_pos,
                &shape,
                QueryFilter::default(),
                |handle| {
                    if let Some(ent) = phys.collider_entity(handle) {
                        mouse.held_ents.insert(ent);
                    }
                    true
                },
            );
            for ent in &mouse.held_ents {
                if let Ok((_, mut stuff_rb, mut spr)) = stuff_query.get_mut(*ent) {
                    spr.color = Color::WHITE;
                    *stuff_rb = RigidBody::KinematicPositionBased;
                }
            }
        }

        if mouse.is_holding {
            // freeze stuff
            if mouse.freeze_ability && mouse.just_pressed_alt {
                for ent in &mouse.held_ents {
                    if let Ok((_, mut rb, mut spr)) = stuff_query.get_mut(*ent) {
                        spr.color = Color::rgb(0.25, 0.25, 0.25);
                        *rb = RigidBody::Fixed;
                    }
                }
                mouse.held_ents.clear();
            } else {
                // drag stuff
                for ent in &mouse.held_ents {
                    if let Ok((mut stuff_trans, _, _)) = stuff_query.get_mut(*ent) {
                        stuff_trans.translation.x += mouse.world_delta.x;
                        stuff_trans.translation.y += mouse.world_delta.y;
                    }
                }
            }
        }

        // let go of stuff
        if mouse.just_released {
            for ent in &mouse.held_ents {
                if let Ok((_, mut stuff_rb, _)) = stuff_query.get_mut(*ent) {
                    *stuff_rb = RigidBody::Dynamic;
                }
            }
            mouse.held_ents.clear();
        }
    }
}
