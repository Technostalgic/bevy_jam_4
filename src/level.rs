use bevy::{prelude::*, window::PrimaryWindow};
use bevy_level_edit::prelude::*;
use bevy_prototype_lyon::{
    draw::Fill, entity::ShapeBundle, geometry::GeometryBuilder, prelude::FillOptions, shapes,
};
use bevy_rapier2d::{
    geometry::{Collider, VHACDParameters},
    parry::math::DIM,
    plugin::RapierConfiguration,
};

use crate::{
    core::GameTime,
    item::{spawn_item_coin, spawn_item_hand, spawn_item_snowflake, ItemsCollected},
    mouse::MouseController,
    stuff::{fill_stuff, StuffData},
    tooltips::{spawn_tooltip, TooltipCondition},
};

// -------------------------------------------------------------------------------------------------

pub struct LevelPlugin;

#[derive(Resource)]
pub struct LoadingLevel(Handle<LevelData>);

#[derive(Component)]
pub struct LevelGeom;

pub trait ExtractIndices {
    fn extract_indices(&self) -> Vec<[u32; DIM]>;
}

// -------------------------------------------------------------------------------------------------

impl Plugin for LevelPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, start_level_loading)
            .add_systems(
                PreUpdate,
                load_level.run_if(resource_exists::<LoadingLevel>()),
            )
            .add_systems(
                OnEnter::<LevelEditorState>(LevelEditorState::Editing),
                to_edit_mode,
            )
            .add_systems(
                OnEnter::<LevelEditorState>(LevelEditorState::Off),
                from_edit_mode,
            );
    }
}

impl ExtractIndices for PolygonData {
    fn extract_indices(&self) -> Vec<[u32; DIM]> {
        let mut vec = Vec::<[u32; DIM]>::new();
        let len = self.verts.len() as u32;
        for i in 1..len {
            vec.push([i, i - 1]);
        }
        if len > 1 {
            vec.push([len - 1, 0]);
        }
        vec
    }
}

// Utility: ----------------------------------------------------------------------------------------

pub fn spawn_geom_from_level_data(commands: &mut Commands, data: &LevelData) {
    let decomp_params = VHACDParameters {
        ..Default::default()
    };
    for poly in &data.poly_data.polys {
        commands.spawn((
            LevelGeom,
            Collider::convex_decomposition_with_params(
                &poly.verts,
                &poly.extract_indices().as_slice(),
                &decomp_params,
            ),
            ShapeBundle {
                spatial: SpatialBundle {
                    transform: Transform::from_translation(poly.translation.extend(10.0)),
                    ..Default::default()
                },
                path: GeometryBuilder::build_as(&shapes::Polygon {
                    points: poly.verts.clone(),
                    closed: true,
                }),
                ..Default::default()
            },
            Fill {
                color: Color::BLACK,
                options: FillOptions::default(),
            },
        ));
    }
}

pub fn spawn_items(commands: &mut Commands) {
    // for easy access / testing
    // commands.spawn(spawn_item_snowflake(Vec2::new(0.0, -10.0)));
    // commands.spawn(spawn_item_hand(Vec2::new(0.0, -10.0)));

    // magic hand
    commands.spawn(spawn_item_hand(Vec2::new(-64.0, 196.0)));

    // snowflake
    commands.spawn(spawn_item_snowflake(Vec2::new(330.0, 300.0)));

    // --- coins ----
    // starting ledge
    commands.spawn(spawn_item_coin(Vec2::new(-80.0, 40.0)));
    commands.spawn(spawn_item_coin(Vec2::new(-90.0, 40.0)));
    commands.spawn(spawn_item_coin(Vec2::new(-180.0, 110.0)));
    // secret left ledge
    commands.spawn(spawn_item_coin(Vec2::new(-480.0, 310.0)));
    commands.spawn(spawn_item_coin(Vec2::new(-460.0, 310.0)));
    // wall dip
    commands.spawn(spawn_item_coin(Vec2::new(155.0, -50.0)));
    commands.spawn(spawn_item_coin(Vec2::new(145.0, -50.0)));
    // secret pit outlet
    commands.spawn(spawn_item_coin(Vec2::new(380.0, -240.0)));
    commands.spawn(spawn_item_coin(Vec2::new(400.0, -240.0)));
    commands.spawn(spawn_item_coin(Vec2::new(420.0, -240.0)));
    // ledge up toward snowflake
    commands.spawn(spawn_item_coin(Vec2::new(825.0, 130.0)));
    // dispenser coin
    commands.spawn(spawn_item_coin(Vec2::new(465.0, 230.0)));
    // 
    commands.spawn(spawn_item_coin(Vec2::new(170.0, 290.0)));
    // garbage chute
    commands.spawn(spawn_item_coin(Vec2::new(900.0, 280.0)));
    commands.spawn(spawn_item_coin(Vec2::new(920.0, 280.0)));
    // catch in chute
    commands.spawn(spawn_item_coin(Vec2::new(1220.0, -300.0)));

}

pub fn spawn_level_stuff(commands: &mut Commands, stuff_data: &Res<StuffData>) {
    
    // trash pit 
    for stuff in fill_stuff(
        Vec2::new(1220.0, -450.0),
        Vec2::new(700.0, -920.0),
        12.0,
        stuff_data.spritesheet.as_ref().unwrap(),
    )
    .into_iter()
    {
        commands.spawn(stuff);
    }

    // top dispenser
    for stuff in fill_stuff(
        Vec2::new(440.0, 500.0),
        Vec2::new(500.0, 340.0),
        12.0,
        stuff_data.spritesheet.as_ref().unwrap(),
    )
    .into_iter()
    {
        commands.spawn(stuff);
    }

    // medium pit
    for stuff in fill_stuff(
        Vec2::new(510.0, 400.0),
        Vec2::new(670.0, -210.0),
        15.0,
        stuff_data.spritesheet.as_ref().unwrap(),
    )
    .into_iter()
    {
        commands.spawn(stuff);
    }

    // first lil dip
    for stuff in fill_stuff(
        Vec2::new(170.0, 35.0),
        Vec2::new(210.0, -15.0),
        12.0,
        stuff_data.spritesheet.as_ref().unwrap(),
    )
    .into_iter()
    {
        commands.spawn(stuff);
    }
    // first lil dip
    for stuff in fill_stuff(
        Vec2::new(100.0, 35.0),
        Vec2::new(140.0, -15.0),
        12.0,
        stuff_data.spritesheet.as_ref().unwrap(),
    )
    .into_iter()
    {
        commands.spawn(stuff);
    }
}

// Systems: ----------------------------------------------------------------------------------------

fn start_level_loading(mut commands: Commands, asset_server: ResMut<AssetServer>) {
    println!("Loading level..");
    commands.insert_resource(LoadingLevel(
        asset_server.load::<LevelData>("level.lvl.json"),
    ));
}

fn load_level(
    mut commands: Commands,
    mut lvl_dat: ResMut<LevelEditorData>,
    stuff_data: Res<StuffData>,
    level_loading: Res<LoadingLevel>,
    level_assets: Res<Assets<LevelData>>,
) {
    if !stuff_data.is_loading && stuff_data.spritesheet.is_some() {
        if let Some(level_data) = level_assets.get(&level_loading.0) {
            commands.remove_resource::<LoadingLevel>();
            lvl_dat.level_data = Some(level_data.clone());
            spawn_geom_from_level_data(&mut commands, &level_data);
            spawn_items(&mut commands);
            spawn_level_stuff(&mut commands, &stuff_data);
            commands.spawn(spawn_tooltip(
                "WASD to move and Space to jump".to_string(),
                TooltipCondition::Flipped,
            ));
            println!("Level Finished Loading!");
        }
    }
}

fn to_edit_mode(
    mut commands: Commands,
    mut rapier_config: ResMut<RapierConfiguration>,
    mut game_time: ResMut<GameTime>,
    mut mouse: ResMut<MouseController>,
    mut window_query: Query<&mut Window, With<PrimaryWindow>>,
    geom_query: Query<Entity, With<LevelGeom>>,
) {
    mouse.active = false;
    window_query.single_mut().cursor.visible = true;
    game_time.pause();
    rapier_config.physics_pipeline_active = false;
    for ent in &geom_query {
        commands.entity(ent).despawn();
    }
    println!("Switched to edit mode");
}

fn from_edit_mode(
    mut commands: Commands,
    mut rapier_config: ResMut<RapierConfiguration>,
    editor_data: Res<LevelEditorData>,
    mut game_time: ResMut<GameTime>,
    mut mouse: ResMut<MouseController>,
    loading_level: Option<Res<LoadingLevel>>,
    collected_items: Res<ItemsCollected>,
) {
    mouse.active = collected_items.magic_hand;
    // only resume game if not loading
    if loading_level.is_none() {
        game_time.start();
        rapier_config.physics_pipeline_active = true;
    }
    if let Some(lvl_dat) = &editor_data.level_data {
        spawn_geom_from_level_data(&mut commands, lvl_dat);
    }
    println!("Switched to gameplay mode");
}
