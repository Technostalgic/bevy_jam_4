mod core;
mod hero;
mod hud;
mod item;
mod level;
mod mouse;
mod stuff;
mod tooltips;

use bevy::prelude::*;
use bevy_asepritesheet::asset_plugin::SpritesheetAssetPlugin;
use bevy_level_edit::prelude::BevyLevelEditPlugin;
use bevy_rapier2d::{
    plugin::{NoUserData, RapierPhysicsPlugin},
    render::RapierDebugRenderPlugin,
};
use bevy_screen_diagnostics::{
    ScreenDiagnosticsPlugin, ScreenEntityDiagnosticsPlugin, ScreenFrameDiagnosticsPlugin,
};
use core::CorePlugin;

fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins.set(ImagePlugin::default_nearest()),
            ScreenDiagnosticsPlugin::default(),
            ScreenFrameDiagnosticsPlugin,
            ScreenEntityDiagnosticsPlugin,
            BevyLevelEditPlugin::new(true, false, Some(KeyCode::F5)),
            SpritesheetAssetPlugin::new(&["sprite.json"]),
            RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(16.0),
            RapierDebugRenderPlugin::default(),
            CorePlugin,
        ))
        .run();
}
